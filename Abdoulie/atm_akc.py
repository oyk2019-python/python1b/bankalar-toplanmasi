import random

details = []
ibanNo = []
saving = [1000]
withdrawal = []
personSignedIn = []
receiverAccount = []
senderAccount = []


def register(name,tckn,parola, verifyParola, mail):

    # inputDetails = {'NAME'=name, 'TCKN'=tckn, 'PAROLA'=parola, 'EMAIL'=mail, 'IBAN'=ibanNo}
    inputDetails = dict(NAME=name, TCKN=tckn, PAROLA=parola, EMAIL=mail, IBAN=ibanNo)
    details.append(inputDetails)

    for i in range(1,25):
        iban = random.randrange(1,9)
        ibanNo.append(iban)

    if parola != verifyParola:
        print("\n\t\t Password Mismatch!! \n\t Please Verify and try again")
        exit()
    elif (len(str(tckn)) != 5):
        print("\n\t\t ID Card Number should be 5 characters !!")
    elif "@" not in mail:
        print("\n\t\t Invalid E-mail \n\t E-mail should include an @ !!")
    else:
        print(details)
        print("IBAN NO: ", *ibanNo, end="")

def save(amount):
    saving.append(amount)

def withdraw(amountToWithdraw):
    withdrawal.append(amountToWithdraw)


def balance():
    accountBalance = sum(saving) - sum(withdrawal)

    print("Current account balance = ", accountBalance)

def transfer(recipient, amount, iban):

    print("\n ENTER SENDERS NAME \n")
    youName = input("Sender's Name: ")

    receiverDict = {
        "RECIPIENT":recipient,
        "AMOUNT":amount,
        "IBAN":iban
    }
    receiverAccount.append(receiverDict)

    accountBalance = sum(saving) - sum(withdrawal)
    senderAccAfterTransfer = accountBalance - amount
    senderDict = {
        "NAME": youName,
        "ACC_BALANCE": senderAccAfterTransfer
    }
    senderAccount.append(senderDict)

    print("SENDER'S ACCOUNT DETAILS = ", senderAccount)
    print("RECEIPIENT ACCOUNT DETAILS = ", receiverAccount)

def login(mail,parola):

    for value in details:

        if (value['EMAIL'] == signInEmail) and (value["PAROLA"] == signInParola):

            print("WELCOME : ", value["NAME"])

            while True:

                print("\n What would you like to do")

                print("""\n
                               1. Save Money
                               2. Withdraw Money
                               3. Check Balance
                               4. Transfer Money
                               5. EXIT
                           """)

                accOption = int(input("Select Option: "))

                if accOption == 1:
                    amount = int(input("How much would you save?: "))
                    save(amount)

                elif accOption == 2:
                    amountToWithdraw = int(input("Amount to withdraw: "))
                    withdraw(amountToWithdraw)

                elif accOption == 3:
                    balance()

                elif accOption == 4:
                    print("\n ENTER RECIPIENT DETAILS \n")
                    recipientName = input("Recipient Name: ")
                    amount = int(input("Amount to send: "))
                    senderIBAN = int(input("Sender I-BAN number: "))

                    transfer(recipientName, amount, senderIBAN)

                elif accOption == 5:
                    exit()
                else:
                    register(name,tckn,parola, verifyParola, mail)

        elif value["EMAIL"] != signInEmail:
            print("\n\t Wrong Email")

        elif value["PAROLA"] != signInParola:
            print("\n\t Wrong Password")
        else:
            pass



print("WELCOME TO GENIAC BANK")

while True:
    print("""\n What would you like to do
        1. Register
        2. Login    
    """)

    option = int(input("Select Option: "))

    if option == 1:

        NAME = input("Name: ")
        TCKN = int(input("ID Card Number: "))
        PAROLA = int(input("Parola: "))
        VERIFYPAROLA = int(input("Verify Parola: "))
        EMAIL = input("E-mail: ")

        register(NAME,TCKN,PAROLA,VERIFYPAROLA ,EMAIL)

    elif option == 2:
        print("\n\t\t\t Enter login info")
        signInEmail = input("Email: ")
        signInParola = int(input("Parola: "))

        login(signInEmail, signInParola)

    else:
        print("\n Invalid entry")
