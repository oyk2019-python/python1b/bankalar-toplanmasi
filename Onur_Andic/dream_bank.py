from random import randint

kisi_listesi = list()

print("--------------- DREAM BANK ---------------")
print("\n")

while True:
    print("""
    1- Giriş Yap
    2- Kayıt Ol
    3- Çıkış
""")
    choose = int(input("yapacağınız işlemi seçin: "))
    
#-------------------------------------------------------giriş------------------------------------------------------------
    
    if choose == 1:
        if len(kisi_listesi) == 0:
            print("henüz hiç bir kayıt bulunmamaktadır. ")
        else:
            TCKN = input("TC girin: ")
            password=input("parolanızı girin: ")
            for kisi in kisi_listesi:
                if kisi["TCKN"] == TCKN and kisi["Parola"] == password:
                    while True:
                       
                        print("""
    1- Para Yatırma
    2- Para Çekme
    3- Transfer
    4- Bakiye Sorgulama
    5- Çıkış
""")
                        secenek = int(input("Yapacağınız işlemi seçin: "))
                        if secenek == 1:
                            miktar = int(input("yatırılacak miktarı girin: "))
                            kisi["Bakiye"] = kisi["Bakiye"] + miktar
                        elif secenek == 2:
                            miktar = int(input("çekilecek miktarı girin: "))
                            kisi["Bakiye"] = kisi["Bakiye"] - miktar
                        elif secenek == 3:
                            transfer_iban = input("transfer edilecek iban numarasını girin: ")
                            transfer_miktar = int(input("transfer edilecek miktarı girin: "))
                            while True:
                                if kisi["Bakiye"] < transfer_miktar:
                                    print("yeterli bakiyeniz bulunmamaktadır. \n")
                                    break
                                else:
                                    kisi["Bakiye"] = kisi["Bakiye"] - transfer_miktar
                                    break
                        elif secenek == 4:
                            print("Bakiyeniz: {}" .format(kisi["Bakiye"]))
                        elif secenek == 5:
                            break
                        else:
                            print("geçerli bir işlem seçmediniz. \nLütfen tekrar deneyin!")
                else:
                    continue
                        
#----------------------------------------------kayıt---------------------------------------------------------------------
    
    elif choose == 2:
        kisi=input("adınızı ve soy adınızı girin:")
        parola=input("lütfen bir parola belirleyin: ")
        while True:
            TC = input("tckn girin:")
            if len(TC) != 6:
                print("TCKN 6 haneli olmalıdır.")
                continue
            else:
                break
        email = input("mail adresinizi girin: ")
        IBAN = random.randint(10000,99999)
        bakiye=1000

        kisi_bilgileri = {
            "ad-soyad" : kisi,
            "Parola" : parola,
            "TCKN" : TC,
            "Mail" : email,
            "iban" : IBAN,
            "Bakiye" : bakiye,
        }
        
        kisi_listesi.append(kisi_bilgileri)
        
        print("""
    kayıt işleminiz tamamlandı.
    Giriş yaparak devam edin.
    """)
        
    elif choose == 3:
        break
    else:
        print("geçerli bir işlem seçin!")
