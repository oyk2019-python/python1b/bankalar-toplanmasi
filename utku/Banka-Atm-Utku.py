"""
...Bank
Banlangıç parası = 1000TL

->Giriş
-Para Yatırma
-Para Çekme
-Bakiye Sorgulama
-Transfer
  -IBAN

->Kayıt
-İsim/Soyisim
-Vatandaşlık Numarası(max 6)
-Parola
-Mail




"""
import random

kullanici_hesaplari = list()


#--------------------------------------------------------------------------
def kayit_alim():
    kullanici_hesaplari.append(kayit_bilgilerini_al())
    print(kullanici_hesaplari)

#--------------------------------------------------------------------------

def kayit_bilgilerini_al():

    isim = input("İsminizi Giriniz : ")
    soyisim = input("Soyisminizi Giriniz : ")
    vatandaslik_numarasi = input("Vatandaslik Numaranizi Giriniz : ")

    while len(vatandaslik_numarasi)>6:
        print("Vatandaslik Numarasi 6 Haneden buyuk olamaz...")
        vatandaslik_numarasi = input("Vatandaslik Numaranizi Giriniz : ")

    parola = input("Parolanizi Giriniz : ")
    mail = input("Mailinizi Giriniz : ")
    bakiye = 1000

    hesap = {
        'isim':isim,
        'soyisim':soyisim,
        'vatandaslik_numarasi':vatandaslik_numarasi,
        'parola':parola,
        'mail':mail,
        'bakiye':bakiye,
    }
    return hesap


#--------------------------------------------------------------------------

while True:
    print("""
        1 - Giris Islemleri
        2 - Kayit Islemleri
    """)
    islem = int(input("Bir İslem Seciniz : "))

    if(islem == 1):#Giris Islemleri
        print("Giris Islemlerini Sectiniz...")
        anahtar = 1#donguyu sonlandırmak ya da devam ettirmek icin
        hak = 3#giris hakki
        while anahtar==1 and hak>0:


            kimlik_numarasi = input("Kimlik Numaranizi Giriniz : ")

            while len(kimlik_numarasi)>6:
                print("Kimlik Numarasi 6 Haneden buyuk olamaz...")
                kimlik_numarasi = input("Vatandaslik Numaranizi Giriniz : ")
            parola = input("Parolanizi Giriniz : ")

            for hesap in kullanici_hesaplari:#girdigimiz kimlik numarasinin ve parolanin hangi hesap oldugunu bulmak ve eslemek icin acilmis bir dongu
                if(kimlik_numarasi == hesap['vatandaslik_numarasi'] and parola==hesap['parola']):
                    anahtar = 0
                    secim = None
                    while secim != 0:
                        print("""
                        1 - Para Yatirma
                        2 - Para Cekme
                        3 - Bakiye Sorgulama
                        4 - Transfer Etme
                        5 - Cikis
                        """)
                        secim = int(input("Bir Secim Yapiniz : "))
                        if(secim == 1):
                            print("Para Yatirma Ekranindasiniz...")
                            miktar = int(input("Hesabiniza Yatiracaginiz Para Miktarini Giriniz : "))
                            hesap['bakiye'] += miktar
                        elif(secim == 2):
                            print("Para Cekme Ekranindasiniz...")
                            miktar = int(input("Hesabinizdan Cekeceginiz Para Miktarini Giriniz : "))
                            if(miktar > hesap['bakiye']):
                                print("Bakiyeniz Yetersiz...")
                            else:
                                hesap['bakiye'] -= miktar

                        elif(secim == 3):
                            print("Bakiye Sorgulama Ekranindasiniz...")
                            print("Hesabinizda Bulunan Para Miktari : {}".format(hesap['bakiye']))


                        elif(secim == 4):
                            print("Transfer Islemi Ekranindasiniz...")
                            miktar = int(input("Transfer Etmek Istediginiz Para Miktarini Giriniz : "))
                            IBAN = random.randint(100000,1000000)
                            print("Paraniz {} Numaralari IBAN'a Gonderildi...".format(IBAN))
                            hesap['bakiye'] -= miktar


                        elif(secim == 0):
                            print("Giris Ekranindan Cikiyorsunuz...")
                            break
                        else:
                            print("Yanlis Secim Yaptiniz ...")


            print("Yanlis Kimlik Numarasi Ya Da Parola Girdiniz ...")
            hak = hak - 1
            print("{} Hakkiniz Kaldi...")
            if(hak==0):
                quit()


        print("Hakkiniz Bitti Iyi Gunler Dileriz...")
        if(hak==0):
            quit()



    elif(islem==2):#Kayit Islemleri...
        print("Kayit Islemlerini Sectiniz...")
        kayit_alim()

    elif(islem==0):#Programdan cikis...
        print("Mutlu Gunler Dileriz...")
        break

    else:#Yanlis islem secimi sonucunda dongunun basina donuluyor...
        print("Yanlis islem yaptiniz...")
