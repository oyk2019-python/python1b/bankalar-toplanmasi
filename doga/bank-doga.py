"""AftonBank Login Platform"""
from random import randint
from bank import Account


users=[]
loggedin=False
running=True
while running:
    a=input("""    AftonBank Web Interface  
Please Register or Login to Continue
    
    1-) Login
    2-) Register
    3-) Exit

>> """)
    if a=="1":
        ID=input("Please Enter Your ID: ")
        pwd=input("Please Enter Your Password: ")
        for i in users:
            if i.ID==ID and i.password==pwd:
                loggedin=True
                print(f"Welcome {i.name}")
                while loggedin:
                    b=input("""    AftonBank Web Interface
Please Choose what do you want to do
    1-) Deposit Money
    2-) Withdraw Money
    3-) Check Balance
    4-) Transfer Money
    5-) Exit

>> """)
                    if b=="1":
                        i.deposit(int(input("Please Enter How Much You Want to Deposit: ")))
                        print(f"Your New Balance is: {i.money}")
                    elif b=="2":
                        amount=int(input("Please Enter How Much You Want to Withdraw: "))
                        if i.money<amount:
                            print("Insufficient Funds")
                        else:
                            i.withdraw(amount)
                            print(f"Your New Balance is: {i.money}")
                    elif b=="3":
                        print(f"Your Balance for {i.accountid}: {i.balcheck()}")
                    elif b=="4":
                        target=input("Please Input Receiver's Account: ")
                        amount=input("Please Input The Amount of Money to Transfer: ")
                        f=False
                        for c in users:
                            if c.accountid==target:
                                f=i.transfer(amount,c)
                        if not f:
                            print("Something Went Wrong")
                    elif b=="5":
                        loggedin=False
                    else:
                        print("Wrong Format")
    elif a=="2":
        name=input("Please Enter Your Name: ")
        surname=input("Please Enter Your Surname: ")
        pwd=input("Please Enter Your Password: ")
        mail=input("Please Enter Your E-Mail: ")
        ID=input("Please Enter Your ID: ")
        if not len(ID)==6:
            while not len(ID)==6:
                ID=input("Your ID Must be 6 Digits Long Please Re-Enter it: ")
        for i in users:
            if ID==i.ID:
                print("You Are Already Registered")
                continue
        acc=Account(name,surname,pwd,mail,ID,users)
        users.append(acc)
        print(f"Your Account ID is: {acc.accountid}")
    elif a=="3":
        loggedin=False
    else:
        print("Wrong Format")
